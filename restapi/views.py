# -*- coding: utf-8 -*-
import json
from lxml import etree
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.generics import get_object_or_404
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from restapi.models import Ticket
from restapi.serializers import UserSerializer, TicketSerializer, FullTicketSerializer
from rest_framework import routers, serializers, viewsets, status, mixins
from django.conf import settings
import requests

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class TicketViewSet(mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.ListModelMixin,
                    GenericViewSet):
    # serializer_class = TicketSerializer
    queryset = Ticket.objects.all()

    def get_serializer_class(self):
        if self.request.user.is_superuser:
            return FullTicketSerializer
        return TicketSerializer
