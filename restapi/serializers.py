from rest_framework import serializers
from restapi.models import Ticket
from django.contrib.auth.models import User
from rest_framework.fields import CurrentUserDefault

STATUSES = ["new", "confirmed"]

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        exclude = ('id', 'owner', 'distributor', 'coupon', 'customer_number')
        read_only_fields = (
            'status', 'created_date', 'modified_date', 'ticket_id', 'delivery_status', 'delivery_label_url',
            'delivery_tracking_url')

class FullTicketSerializer(TicketSerializer):
    """This serializer is for superuser, which can write anything."""

    class Meta:
        model = Ticket
        exclude = ('id', 'customer_number')
