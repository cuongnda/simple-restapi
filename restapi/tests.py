from django.test import TestCase

# Create your tests here.
from django_webtest import WebTest
# from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group, Permission
from django_dynamic_fixture import G, N
from django.conf import settings
from rest_framework.reverse import reverse
from restapi.models import Ticket
from rest_framework.test import APIClient

DP_GROUP_PERMISSIONS = [
    'Can add ticket',
    'Can change ticket',
    'Can delete ticket',
]


class TestTicketResourceAdmin(WebTest):
    csrf_checks = False

    def setUp(self):
        self.dp_group = G(Group)
        self.superuser = G(User, is_superuser=True)
        self.staff = G(User, is_staff=True)
        self.customer = G(User, username='10000', is_staff=False)
        for permission_name in DP_GROUP_PERMISSIONS:
            permission = Permission.objects.get(name=permission_name)
            self.dp_group.permissions.add(permission)
        self.dp = G(User, username='dp1', is_staff=False)
        self.dp_group.user_set.add(self.dp)
        self.ticket1 = G(Ticket, package=u'{}', shop_order_id="order1", owner=self.dp,)
        self.ticket2 = G(Ticket, package=u'{}', shop_order_id="order2", owner=self.dp,)

    def test_ticket_to_string(self):
        assert str(self.ticket1) == u"%s %s %s from %s" % (
            self.ticket1.customer_title, self.ticket1.customer_first_name, self.ticket1.customer_last_name,
            self.ticket1.owner.username)
        ticket3 = G(Ticket, package=u'{}', shop_order_id="order3", customer_title='Mr', customer_first_name="John",
                    customer_last_name="Smith", owner=None,)
        assert str(ticket3) == u"%s %s %s" % (
            ticket3.customer_title, ticket3.customer_first_name, ticket3.customer_last_name)

    def test_get_ticket_details_unauthorized(self):
        response = self.app.get(reverse('tickets-detail', args=(self.ticket1.shop_order_id,)), status=401)
        assert response.json['detail'] == u'Authentication credentials were not provided.'

    def test_listing_tickets(self):
        response = self.app.get(reverse('tickets-list'), user=self.dp)
        tickets = response.json
        assert response.status_int == 200
        assert len(tickets) == 2

    def test_add_ticket(self):
        ticket_payload = {
            "customer_title": "Her",
            "customer_first_name": "John",
            "customer_last_name": "Smith",
            "customer_company": "Wayfair",
            "customer_address": "Osloer Strasse 12",
            "customer_address_comment": "Back yard",
            "customer_city": "Berlin",
            "customer_zipcode": "13359",
            "customer_country": "de",
            "customer_email": "email@example.com",
            "customer_telephone1": "123465798",
            "customer_telephone2": "",
            "customer_telephone3": "",
            "customer_delivery_title": "Her",
            "customer_delivery_first_name": "John",
            "customer_delivery_last_name": "Smith",
            "customer_delivery_company": "",
            "customer_delivery_address": "Osloer Strasse 12",
            "customer_delivery_address_comment": "Back yard",
            "customer_delivery_city": "Berlin",
            "customer_delivery_zipcode": "13359",
            "customer_delivery_country": "de",
            "customer_delivery_telephone1": "13456789",
            "customer_delivery_telephone2": "",
            "customer_delivery_telephone3": "",
            "shop_product": "Sony TV 50 inch",
            "description": "",
            "internal_description": "",
            "shop_order_id": "MYORDER_Nr1",
            "delivery_date": "2017-01-03",
            "payment_method": "pay_by_customer",
            "package": '[{"package_ean": "1234567891011"}, {"package_ean": "1234567891011"}]'
        }
        response = self.app.post(reverse('tickets-list'), user=self.dp, params=ticket_payload)
        assert response.status_int == 201
        assert response.json['customer_title'] == "Her"

    def test_add_ticket_forbiden(self):
        ticket_payload = {
            "customer_title": "Her",
            "customer_first_name": "John",
            "customer_last_name": "Smith",
            "customer_company": "Wayfair",
            "customer_address": "Osloer Strasse 12",
            "customer_address_comment": "Back yard",
            "customer_city": "Berlin",
            "customer_zipcode": "13359",
            "customer_country": "de",
            "customer_email": "email@example.com",
            "customer_telephone1": "123465798",
            "customer_telephone2": "",
            "customer_telephone3": "",
            "customer_delivery_title": "Her",
            "customer_delivery_first_name": "John",
            "customer_delivery_last_name": "Smith",
            "customer_delivery_company": "",
            "customer_delivery_address": "Osloer Strasse 12",
            "customer_delivery_address_comment": "Back yard",
            "customer_delivery_city": "Berlin",
            "customer_delivery_zipcode": "13359",
            "customer_delivery_country": "de",
            "customer_delivery_telephone1": "13456789",
            "customer_delivery_telephone2": "",
            "customer_delivery_telephone3": "",
            "shop_product": "Sony TV 50 inch",
            "description": "",
            "internal_description": "",
            "shop_order_id": "MYORDER_Nr1",
            "payment_method": "pay_by_customer",
            "package": '[{"package_ean": "1234567891011"}, {"package_ean": "1234567891011"}]'
        }
        response = self.app.post(reverse('tickets-list'), user=self.customer, params=ticket_payload, status=403)
        assert response.json['detail'] == u'You do not have permission to perform this action.'
