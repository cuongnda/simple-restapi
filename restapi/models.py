from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models
from django.contrib.postgres.fields import JSONField

PAYMENT_METHODS = (
    (u'pay_by_customer', u'pay_by_customer'),
    (u'pay_by_distributor', u'pay_by_distributor'),
)

DELIVERY_STATUS = (
    (u'picked_up', 'picked_up'),
    (u'delivered', 'delivered'),
)

# Create your models here.
class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ('created_date',)


class Ticket(BaseModel):
    ticket_id = models.CharField(max_length=100, blank=True, default='')
    customer_number = models.CharField(max_length=100, blank=True, null=False,
                                       help_text=u'An unique ID for a customer')
    customer_title = models.CharField(max_length=100, blank=False, null=False, help_text=u'Customer title')
    customer_first_name = models.CharField(max_length=200, blank=False, null=False, help_text=u'Customer first name')
    customer_last_name = models.CharField(max_length=200, blank=False, null=False, help_text=u'Customer last name')
    customer_company = models.CharField(max_length=200, blank=True, default='', help_text=u'Customer company')
    customer_address = models.CharField(max_length=200, blank=False, null=False, help_text=u'Customer address')
    customer_address_comment = models.CharField(max_length=200, blank=True, default='',
                                                help_text=u'Instruction to customer address. E.g back yard')
    customer_city = models.CharField(max_length=100, blank=False, null=False, help_text=u'Customer city')
    customer_zipcode = models.CharField(max_length=100, blank=False, null=False, help_text=u'Customer zipcode')
    customer_country = models.CharField(max_length=100, blank=False, null=False,
                                        help_text=u'Customer country 2 character ISO code. E.g de for Germany')
    customer_telephone1 = models.CharField(max_length=100, blank=False, null=False,
                                           help_text=u'Customer telephone number 1')
    customer_telephone2 = models.CharField(max_length=100, blank=True, default='',
                                           help_text=u'Customer telephone number 2')
    customer_telephone3 = models.CharField(max_length=100, blank=True, default='',
                                           help_text=u'Customer telephone number3')
    customer_email = models.CharField(max_length=100, blank=False, null=False)

    customer_delivery_title = models.CharField(max_length=100, blank=False, null=False)
    customer_delivery_first_name = models.CharField(max_length=200, blank=False, null=False)
    customer_delivery_last_name = models.CharField(max_length=200, blank=False, null=False)
    customer_delivery_company = models.CharField(max_length=200, blank=True)
    customer_delivery_address = models.CharField(max_length=200, blank=False, null=False)
    customer_delivery_address_comment = models.CharField(max_length=200, blank=True, default='')
    customer_delivery_city = models.CharField(max_length=100, blank=False, null=False)
    customer_delivery_zipcode = models.CharField(max_length=100, blank=False, null=False)
    customer_delivery_country = models.CharField(max_length=100, blank=False, null=False)
    customer_delivery_telephone1 = models.CharField(max_length=100, blank=False, null=False)
    customer_delivery_telephone2 = models.CharField(max_length=100, blank=True, default='')
    customer_delivery_telephone3 = models.CharField(max_length=100, blank=True, default='')

    shop_product = models.CharField(max_length=100, blank=True, default='',
                                    help_text=u'Title of product in the shop for references')
    description = models.CharField(max_length=100, blank=True, default='',
                                   help_text=u'General description of the ticket')
    internal_description = models.CharField(max_length=100, blank=True, default='',
                                            help_text=u'Description which is only visible to Get it done and'
                                                      u' service partner')
    shop_order_id = models.CharField(max_length=100, blank=False,
                                     null=False, help_text=u'Order ID in the shop system')
    distributor = models.CharField(max_length=100, blank=True, default='',
                                   help_text=u'UID of distribution partner in GIDSYS. '
                                             u'This value should be set by GIDAPI itself.')
    payment_method = models.CharField(max_length=100, blank=False, null=False, choices=PAYMENT_METHODS,
                                      help_text=u'Payment method of this ticket')
    coupon = models.CharField(max_length=100, blank=True, default='', help_text=u'Coupon used in this ticket')
    package = JSONField(blank=False, null=False,
                        help_text=u'JSON code for package information. Example: '
                                  u'[{\"packge_ean\":\"EAN1\",'
                                  u'\"package_variation\": {\"option1\":\"option1_value\",'
                                  u'\"option2\":\"option2_value\"}},'
                                  u'{\"packge_ean\":\"EAN2\",'
                                  u'\"package_variation\": {\"option1\":\"option21_value\",'
                                  u'\"option2\":\"option22_value\"}}], ')

    status = models.CharField(max_length=100, blank=True, default='', help_text=u'Ticket status.')

    owner = models.ForeignKey(User, blank=True, null=True)

    delivery_date = models.DateField(blank=True, null=True,
                                     help_text=u'The ISO 8601 (yyyy-MM-dd) date of the delivery which '
                                               u'product can be picked up from the shop')

    delivery_status = models.CharField(max_length=100, blank=True, null=True, choices=DELIVERY_STATUS,
                                       help_text=u'Delivery status of this order')
    delivery_label_url = models.URLField(blank=True, null=True, help_text=u'URL to the delivery label.')

    delivery_tracking_url = models.URLField(blank=True, null=True, help_text=u'URL to the order tracking page.')
    weight = models.IntegerField(blank=True,
                                 null=True, help_text=u'Weight of the delivery parcel in gram')
    size = JSONField(blank=True, null=True,
                     help_text=u'JSON code for 3 dimension of the delivery parcel in millimeter. '
                               u'Example: [1000, 800, 900]')

    def __str__(self):
        if self.owner:
            return u"%s %s %s from %s" % (
                self.customer_title, self.customer_first_name, self.customer_last_name, self.owner.username)
        else:
            return u"%s %s %s" % (
                self.customer_title, self.customer_first_name, self.customer_last_name)
