from django.conf.urls import url, include
from restapi import views
from restapi.views import UserViewSet, TicketViewSet
from rest_framework import routers, serializers, viewsets


# Routers provide an easy way of automatically determining the URL conf.

router = routers.DefaultRouter()
#router.register(r'users', UserViewSet)
router.register(r'tickets', TicketViewSet, 'tickets')


urlpatterns = [
    url(r'^', include(router.urls)),
]