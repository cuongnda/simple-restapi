from gidapi.settings.base import *

# INSTALLED_APPS += ('debug_toolbar',)
#
# MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
#
# # The Django Debug Toolbar will only be shown to these client IPs.
# INTERNAL_IPS = (
#     '127.0.0.1',
# )
ALLOWED_HOSTS = ('127.0.0.1', '192.168.33.15')

# DEBUG_TOOLBAR_CONFIG = {
#     'INTERCEPT_REDIRECTS': False,
#     'SHOW_TEMPLATE_CONTEXT': True,
#     'HIDE_DJANGO_SQL': False,
# }

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'simpleapi',
        'USER': 'cuongnda',
        'PASSWORD': '111111',
        'HOST': 'localhost',
        'PORT': '',
    }
}
