GIDAPI is REST API for GIDSYS, allow our partners integrate to our system

1. Quickstart
   - Install dependencies:
        pip install -r requirements.txt
   - Setup database: edit database setting in gidapi.settings.local
   - initial migration
        python manage.py migrate --settings=gidapi.settings.local
   - Start server 
        python manage.py migrate --settings=gidapi.settings.local
   - Test: Access browser on : 127.0.0.1:8000 
   
2. Deployment
   Check Readme in ansible folder for details